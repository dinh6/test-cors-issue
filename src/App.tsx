/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useState } from "react";
import "./App.css";

function App() {
  const [res, setRes] = useState<any>();
  const [err, setErr] = useState<any>();

  useEffect(() => {
    fetch(
      "https://api-public.hubble.build/waf-api/rails/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWs1WlRoak5qTmtZUzB4TlRJNExUUXlaRFV0WVRnMU55MWlPVE0zTVRaaFlXSmlNVGNHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--940aa6aa7bf69bc517cd6e94cce9387162953925/HDEC-EL-SHD-2500-01-01%20(1)%20LPS%20Dwgs%20).pdf"
    )
      .then((res) => {
        res.json().then((data) => {
          console.log("data", data);
          setRes(data);
        });
      })
      .catch((e) => {
        console.log("err", e);
        setErr(JSON.stringify(e.message));
      });
  }, [setRes, setErr]);

  return (
    <>
      <div>Success: {JSON.stringify(res)}</div>
      <div>Error: {JSON.stringify(err)}</div>
    </>
  );
}

export default App;
